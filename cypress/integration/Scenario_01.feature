Feature: Connexion

    Je veux me connecter à shareplace

    Background: Résolution
        Given Résolution : 1920 x 1080

    Scenario: Ouvrir la page de connexion
        Given Je visite la page de connexion
        Then La page de connexion s'affiche

    Scenario: Se connecter
        Given Je suis sur la page de connexion
        When Je saisis mon email dans le champ E-mail
            And Je saisis "123456" dans le champ Mot de passe
            And Je clique sur le bouton Se connecter
        Then Je suis connecté et je vois la page principale s'afficher