/// <reference types="cypress" />

import { Given } from "cypress-cucumber-preprocessor/steps"

//BACKGROUND
Given(`Résolution : {int} x {int}`, (x, y) => {
    cy.viewport(x, y)
})